#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

extern crate oauth2;
extern crate url;
extern crate reqwest;
extern crate serde;

use std::collections::HashMap;
use std::str;
use std::string::String;
use rocket::http::{Cookie, Cookies};
use rocket::fairing::AdHoc;
use rocket_contrib::templates::Template;
use oauth2::{ClientId, ClientSecret, AuthUrl, TokenUrl, RedirectUrl};
use oauth2::basic::BasicClient;
use url::Url;

use crate::structs::HttpClient;
use crate::twitch_auth::structs::{OAuth, Client};

mod twitch_auth;
mod structs;

#[get("/")]
fn index(mut cookies: Cookies) -> Template {
    let cookie = cookies.get_private("state");
    let cookie = cookie.unwrap_or(Cookie::new("state", "default"));

    let mut map = HashMap::new();
    map.insert("name", cookie.value());
    Template::render("hello", map)
}

#[get("/<name>")]
fn name(name: String, mut cookies: Cookies) -> &'static str {
    let cookie = Cookie::new("test", name);
    cookies.add_private(cookie);
    return "Name successfully set..."
}

fn get_custom_option(rocket: &rocket::Rocket, name: &str) -> String {
    rocket.config()
        .get_str(name)
        .expect(format!("Config option '{}' not provided!", name.to_string()).as_str())
        .to_string()
}

fn main() {
    rocket::ignite()
        .attach(Template::fairing())
        .mount("/", routes![index,name,twitch_auth::routes::auth_twitch,twitch_auth::routes::auth_twitch_callback])
        .attach(AdHoc::on_attach("Reqwest Http Client", |rocket| {
            Ok(rocket.manage(HttpClient(reqwest::Client::new())))
        }))
        .attach(AdHoc::on_attach("Twitch OAuth Client", |rocket| {
            let client_id = get_custom_option(&rocket, "twitch_client_id");
            let client_secret = get_custom_option(&rocket, "twitch_client_secret");
            let redirect_uri = get_custom_option(&rocket, "twitch_redirect_uri");
            let client_id = ClientId::new(client_id);
            let client_secret = ClientSecret::new(client_secret);

            let base_url = "https://id.twitch.tv";
            let mut auth_url = "/oauth2/authorize".to_owned();
            auth_url.insert_str(0, base_url);
            let auth_url = AuthUrl::new(Url::parse(auth_url.as_str()).unwrap());
            let mut token_url = "/oauth2/token".to_owned();
            token_url.insert_str(0, base_url);
            let token_url = TokenUrl::new(Url::parse(token_url.as_str()).unwrap());

            let client = BasicClient::new(
                client_id, Some(client_secret), 
                auth_url, Some(token_url))
                .set_redirect_url(RedirectUrl::new(
                        Url::parse(redirect_uri.as_str()).expect("Invalid redirect uri")
            ));
            Ok(rocket.manage(Client(client)))
         }))
        .attach(AdHoc::on_attach("Twitch OAuth Config", |rocket| {
            let client_id = get_custom_option(&rocket, "twitch_client_id");
            let client_secret = get_custom_option(&rocket, "twitch_client_secret");
            let redirect_uri = get_custom_option(&rocket, "twitch_redirect_uri");
            let scope = get_custom_option(&rocket, "twitch_scope");
            Ok(rocket.manage(OAuth{scope, client_secret, client_id,redirect_uri }))
        }))
        .launch();
}
