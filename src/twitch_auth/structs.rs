use oauth2::basic::BasicClient;
use serde::Deserialize;

pub struct OAuth{pub scope: String, pub client_secret: String, pub client_id: String, pub redirect_uri: String}
pub struct Client(pub BasicClient);

#[derive(Deserialize)]
pub struct TwitchToken {
    pub access_token: String,
    pub refresh_token: String,
    pub expires_in: u64,
    pub scope: Vec<String>,
    pub token_type: String
}
