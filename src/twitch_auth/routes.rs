use rocket::http::{Cookie, Cookies, SameSite};
use rocket::State;
use rocket::response::Redirect;
use oauth2::{CsrfToken, Scope};

use crate::twitch_auth::structs::{OAuth, Client, TwitchToken};
use crate::structs::HttpClient;

#[get("/auth/twitch")]
pub fn auth_twitch(mut cookies: Cookies, oauth: State<OAuth>, client: State<Client>) -> Redirect {
    let (authorize_url, state) = client.0
        .authorize_url(CsrfToken::new_random)
        .add_scope(Scope::new(oauth.scope.clone()))
        .url();

    let state = Cookie::build("state", state.secret().clone()).same_site(SameSite::Lax).finish();
    cookies.add_private(state);

    Redirect::to(authorize_url.to_string())
}


#[get("/auth/twitch/callback?<state>&<code>")]
pub fn auth_twitch_callback(state: String, code: String, mut cookies: Cookies, oauth: State<OAuth>, client: State<HttpClient>) -> String {
    let state_cookie = cookies.get_private("state");
    let state_cookie = state_cookie.unwrap_or(Cookie::new("state", "default"));
    if state_cookie.value() == state.as_str() {
        //        let token = client.0.exchange_code(AuthorizationCode::new(code))
        //            .add_extra_param("redirect_uri", oauth.redirect_uri.clone())
        //            .add_extra_param("client_id", oauth.client_id.clone())
        //            .add_extra_param("client_secret", oauth.client_secret.clone()).request(http_client);
        //       let token = token.unwrap();
        //        let token = token.access_token();
        //        let token = Cookie::new("token", token.secret().clone());


        let mut token = client.0.post("https://id.twitch.tv/oauth2/token")
            .query(&[("client_id", oauth.client_id.clone()),
            ("client_secret", oauth.client_secret.clone()),
            ("code", code),
            ("grant_type", "authorization_code".to_owned()),
            ("redirect_uri", oauth.redirect_uri.clone())]).send().unwrap();
        let token: TwitchToken = token.json().unwrap();
        let token  = Cookie::new("token", token.access_token);
        cookies.add_private(token.clone());
        return token.value().to_string();
    } else {
        cookies.remove_private(state_cookie.clone());
        return state + "state mismatch" + state_cookie.value();
    }
}

